<?php
/*
plugin Name:Awebon ROI Quiz
plugin url:http://www.awebon.com/
version:1.0
author:Karthikeyan Balasubramanian
author url:http://karthik.awebon.com/
Description: This plugin calculates the ROI of your Marketing Campaign
*/

function displayQuiz()
{
  
       // wp_register_style( 'bootstrap-css','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css', array(), '3.3.6' );
       // wp_enqueue_style( 'bootstrap-css');
        wp_register_style( 'animate-css', plugin_dir_url( __FILE__ ) . 'css/animate.css', array(), '1.1' );
        wp_enqueue_style( 'animate-css');
        wp_register_style( 'awebon-roi', plugin_dir_url( __FILE__ ) . 'css/awebon-roi.css', array(), '1.1' );
        wp_enqueue_style( 'awebon-roi');
    $displayQuiz = '<script type="text/javascript">
var ajaxurl = \'' . admin_url("admin-ajax.php") . '\';
</script>';
    $rr = plugins_url('',__FILE__);
    $displayQuiz .= <<<EOT
    <script>
       jQuery(function() {
          showhide(1);                                    
       });

       function showhide(v) {
          for (var i = 1; i <= 5; i++) {
             if (v == i) {
                jQuery(".q"+i).show();
             } else {
                jQuery(".q"+i).hide();
             }
          }
       }

       function setHiddenValue(h,v) {
             jQuery('#'+h).val(v);
             switch (h) {
                case "text1":
                   showhide(2);  
                   break;
                case "text2":
                   showhide(3);
                   break;
                case "text3":
                   showhide(4);
                   break;
                case "text4":
                   showhide(5);
                   ajaxCallCalculate();
                   break;
                default:
                   break;
             }  
          }
       </script>
    <form id="calcontact" method="post" >
    <div class="awebon-container awebon-roibg">
        <div class="q1 awebon-row awebon-quizbox fadeInLeftBig">
              <div class="awebon-col-xs-12 awebon-col-lg-12 quizheading animated tada">
                Calculate Your Return Of Investment<br>
                <p class="saving"><span>. </span><span>. </span><span>. </span></p>
              </div>
              <div class="awebon-col-xs-12 awebon-col-lg-12 quizquestion animated bounceInLeft">
              How Many Mailers Sent?
              </div>
             <div class="awebon-col-xs-12 awebon-col-lg-6 animated bounceInLeft ans-img">
                   <div class="ans-container">
                    <img src="{$rr}/images/group.png" alt="Email Icon" class="smallicon animated swing" />
                    <input type="button" value="5,000" class="quizanswer animated bounceInLeft orange-flat-button" onclick="setHiddenValue('text1','5000')">
                   </div>
                   <div class="ans-container">
                   <img src="{$rr}/images/group.png" alt="Email Icon" class="smallicon animated swing" />
                   <input type="button" value="10,000" class="quizanswer animated bounceInLeft orange-flat-button" onclick="setHiddenValue('text1','10000')">
                  </div>
                  <div class="ans-container">
                   <img src="{$rr}/images/group.png" alt="Email Icon" class="smallicon animated swing" /><input type="button" value="20,000" class="quizanswer animated bounceInLeft orange-flat-button" onclick="setHiddenValue('text1','20000')">
                   </div>
                   <input type="hidden" name="text1" id="text1" value="" />
             </div>
             <div class=" awebon-hidden-xs awebon-hidden-sm awebon-col-lg-6 animated bounceInLeft ans-img">
              <img src="{$rr}/images/eicon.png" alt="Email Icon" class="imgeicon animated shake"/>
             </div>            
        </div>
        <div class="q2 awebon-row awebon-quizbox fadeInLeftBig">
           <div class="awebon-col-xs-12 awebon-col-lg-12 quizheading animated tada">
             Calculate Your Return Of Investment<br>
             <p class="saving"><span>. </span><span>. </span><span>. </span></p>
             </div>
             <div class="awebon-col-xs-12 awebon-col-lg-12 quizquestion animated bounceInLeft">
             Revenue Per Implant Case?
             </div>
             <div class="awebon-col-xs-12 awebon-col-lg-8 animated bounceInLeft ans-img">

                    <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">Single Tooth Implant</div>
                   <div><img src="{$rr}/images/bag.png" alt="Money Bag Icon" class="smallicon animated swing" />
                   <input type="button" value="Single Tooth Implant $3000" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text2','3000')" >                   
                   <input type="button" value="$3000" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text2','3000')" >
                  </div>
             
                    <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">Overdenture Case</div>                   
                   <div><img src="{$rr}/images/bag.png" alt="Email Icon" class="smallicon animated swing" />
                   <input type="button" value="Overdenture Case $8,500" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text2','8500')" >
                   <input type="button" value="$8,500" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text2','8500')" >
                   </div>

             
                    <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">Single Arch Case</div>                   
                   <div><img src="{$rr}/images/bag.png" alt="Email Icon" class="smallicon animated swing" />
                   <input type="button" value="Single Arch Case $24,000" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text2','24000')" >
                   <input type="button" value="$24,000" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text2','24000')" >
                   </div>

             
                    <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">Full Arch Case</div>                   
                   <div><img src="{$rr}/images/bag.png" alt="Email Icon" class="smallicon animated swing" />
                   <input type="button" value="Full Arch Case $45,000" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text2','45000')" >
                    <input type="button" value="$45,000" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text2','45000')" >
                    </div>
                   <input type="hidden" name="text2" id="text2" value="" />
             </div>
             <div class="awebon-hidden-xs awebon-hidden-sm awebon-col-lg-4 animated bounceInLeft ans-img">
              <img src="{$rr}/images/gmoney.png" alt="Get Money Icon" class="big-imgeicon animated shake"/>
             </div>        
        </div>
        <div class="q3 awebon-row awebon-quizbox fadeInLeftBig">
           <div class="awebon-col-xs-12 awebon-col-lg-12 quizheading2 animated tada">
             Calculate Your Return Of Investment<br>
             <p class="saving"><span>. </span><span>. </span><span>. </span></p>
             </div>
             <div class="awebon-col-xs-12 awebon-col-lg-12 quizquestion2 animated bounceInLeft">
             Average Mailer Call Rate?
             </div>
             <div class="awebon-col-xs-12 awebon-col-lg-6 animated bounceInLeft ans-img">
                   <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">Just Below Average</div>
                   <div><img src="{$rr}/images/percentage.png" alt="Email Icon" class="smallicon animated swing" />
                   <input type="button" value="Just Below Average" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text3','0.002')">
                   <input type="button" value="0.2%" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text3','0.002')">
                   </div>

                  <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">Average</div>
                  <div><img src="{$rr}/images/percentage.png" alt="Email Icon" class="smallicon animated swing" />
                  <input type="button" value="Average" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text3','0.004')">
                  <input type="button" value="0.4%" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text3','0.004')">
                  </div>

                   <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">Above Average</div>
                   <div>
                   <img src="{$rr}/images/percentage.png" alt="Email Icon" class="smallicon animated swing" />
                   <input type="button" value="Above Average" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text3','0.006')">
                   <input type="button" value="0.6%" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text3','0.006')">
                   </div>

                   <input type="hidden" name="text3" id="text3" value="" />
             </div>
             <div class="awebon-hidden-xs awebon-hidden-sm awebon-col-lg-6 animated bounceInLeft ans-img">
              <img src="{$rr}/images/callback.png" alt="Mailer Call Back Icon" class="imgeicon animated shake"/>
             </div>          
        </div>
        <div class="q4 awebon-row awebon-quizbox fadeInLeftBig">
             <div class="awebon-col-xs-12 awebon-col-lg-12 quizheading2 animated tada">
               Calculate Your Return Of Investment<br>
               <p class="saving"><span>. </span><span>. </span><span>. </span></p>
               </div>
               <div class="awebon-col-xs-12 awebon-col-lg-12 quizquestion2 animated bounceInLeft">
               Average Call To New Patient Rate?
               </div>
               <div class="awebon-col-xs-12 awebon-col-lg-8 animated bounceInLeft ans-img">

               
                     <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">Low Conversion Rate</div>
                     <div>
                      <img src="{$rr}/images/percentage.png" alt="Email Icon" class="smallicon animated swing" />
                      <input type="button" value="Low Conversion Rate 30%" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text4','30')">
                      <input type="button" value="30%" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text4','30')">
                     </div>
                     
                     <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">Average Conversion Rate</div>
                     <div>
                      <img src="{$rr}/images/percentage.png" alt="Email Icon" class="smallicon animated swing" />
                      <input type="button" value="Average Conversion Rate 55%" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text4','55')">
                      <input type="button" value="55%" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text4','55')">
                     </div>

                     <div class="ans-heading awebon-hidden-ls awebon-hidden-sm">High Conversion Rate</div>
                     <div>
                      <img src="{$rr}/images/percentage.png" alt="Email Icon" class="smallicon animated swing" />
                      <input type="button" value="High Conversion Rate 70%" class="awebon-hidden-xs quizanswer2 orange-flat-button" onclick="setHiddenValue('text4','70')">
                      <input type="button" value="70%" class="awebon-hidden-sm awebon-hidden-ls quizanswer2 orange-flat-button" onclick="setHiddenValue('text4','70')">
                     </div>


                     <input type="hidden" name="text4" id="text4" value="" />
               </div>
               <div class="awebon-hidden-xs awebon-hidden-sm awebon-col-lg-4 animated bounceInLeft ans-img">
                <img src="{$rr}/images/chat.png" alt="Chat Icon" class="big-imgeicon animated shake"/>
               </div>          
        </div>
        <div class="q5 awebon-row awebon-quizbox">
             <div class="awebon-col-xs-12 awebon-col-lg-12 quizheading2 animated tada">
               Your Return Of Investment<br>
               <p class="saving"><span>. </span><span>. </span><span>. </span></p>
               </div>
               <div class="awebon-col-xs-12 awebon-col-lg-12 quizquestion2 animated bounceInLeft awebon-roi-result-box">
               <span id="returninvesment" ></span> for every $1 invested
               </div>
                 <div class="awebon-col-xs-12 awebon-col-lg-6 animated bounceInLeft ans-img awebon-roi-graph-container awebon-text-center">
                    <div><canvas id="cvs" width="300" height="250" >[No canvas support]</canvas></div>
                    <div class="legend-container">
                      <div class="legend1"></div>&nbsp;<span>Total Revenue Generated</span>
                      <div class="legend2"></div>&nbsp;<span>Total Cost</span>                                           
                    </div>
                </div>
               <div class="awebon-col-xs-12 awebon-col-lg-6 animated bounceInLeft ans-img">
                   <div class="displayresult">Total Cost : <span id="roiop" ></span></div>
                   <div class="displayresult">Average # of Phone Calls : <span id="avgphcall" ></span></div>
                   <div class="displayresult">Number of New Patients : <span id="nonewpatient" ></span></div>
                   <div class="displayresult">Total Revenue Generated: <span id="totalrevenue" ></span></div>
              <div id="displayresult" class="roi-display">Campaign ROI: <span id="roi" style="float: right; padding-right: 25px; display: inline;"></span></h3>
               </div>
               <input type="hidden" name="text5" id="text5" value="" />          
        </div>
        <input type="hidden" name="action" value="calculateROIAction"/> 
     </div>
    </form>       
EOT;
    
    
    wp_register_script('loader-js', 'https://www.gstatic.com/charts/loader.js', array(
        'jquery'
    ), null, true);
    wp_enqueue_script('loader-js');
    wp_register_script('awebon-quiz-js', plugins_url('js/awebon_quiz.js', __FILE__), array(
        'jquery'
    ), null, true);
    wp_enqueue_script('awebon-quiz-js');

    wp_register_script('RGraph.common.core.js', plugins_url('js/RGraph.common.core.js', __FILE__), array('jquery'), null, true);
    wp_enqueue_script('RGraph.common.core.js');
    wp_register_script('RGraph.common.key.js', plugins_url('js/RGraph.common.key.js', __FILE__), array('jquery'), null, true);
    wp_enqueue_script('RGraph.common.key.js');
    wp_register_script('RGraph.pie.js', plugins_url('js/RGraph.pie.js', __FILE__), array('jquery'), null, true);
    wp_enqueue_script('RGraph.pie.js');
    
    return $displayQuiz;
}

add_shortcode('awebon_ROI_Quiz', 'displayQuiz');



function calculateROIFunction()
{
    $text1 = $_POST['text1'];
    $text2 = $_POST['text2'];
    $text3 = $_POST['text3'];
    $text4 = $_POST['text4'];
    if ($text1 == 5000) {
        $roi = $text1 * 0.80;
    } else if ($text1 == 10000) {
        $roi = $text1 * 0.70;
    } else {
        $roi = $text1 * 0.63;
    }
    $avgphonecalls      = $text1 * $text3;
    $numberofnewpatient = $avgphonecalls * $text4 / 100;
    $revenuegenarate    = $numberofnewpatient * $text2;
    $returnofinves      = $revenuegenarate / $roi;
    $result             = array(
        'roi' => $roi,
        'avgphone' => $avgphonecalls,
        'nonewpatient' => $numberofnewpatient,
        'revenuegenarate' => $revenuegenarate,
        'returninvesment' => round($returnofinves,2)
    );
    echo json_encode($result);
    die();
}
add_action('wp_ajax_calculateROIAction', 'calculateROIFunction');
add_action('wp_ajax_nopriv_calculateROIAction', 'calculateROIFunction');
?>