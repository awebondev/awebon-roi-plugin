function ajaxCallCalculate(){
				jQuery.post(ajaxurl,jQuery("#calcontact").serialize(), function(data){
					var result = JSON.parse(data);
					jQuery("#roiop").html('$<b>'+result.roi+'</b>');
					jQuery("#avgphcall").html('<b>'+result.avgphone+'</b>');
					jQuery("#nonewpatient").html('<b>'+result.nonewpatient+'</b>');
					jQuery("#totalrevenue").html('$<b>'+result.revenuegenarate+'</b>');
					jQuery("#returninvesment").html('$<b>'+result.returninvesment+'</b>');
					jQuery("#roi").html('<b>'+result.returninvesment+'</b>%');
					//setTimeout( function(){ 
					    drawChart(result.revenuegenarate,result.roi);
					  //}  , 1000 );
					
				});
				return false;
}
/* google.charts.load("current", {packages:["corechart"]});
		      function drawChart(val1=0,val2=0) {
		        var data = google.visualization.arrayToDataTable([
		          ['Name', 'value'],
		          ['Total Revenue Generated',   val1],
		          ['Total Cost For Campaign',   val2]
		        ]);

		        var options = {
		          width: 400,
		          backgroundColor: '#D8D8D8',
		          is3D: true,
				  chartArea:{width:'90%',height:'75%'},
				  slices: {  1: {offset: 0.15}},
				  pieSliceText: 'value',
				  pieStartAngle: 100,
		        };

		        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
		        chart.draw(data, options);
		    }*/
		    function drawChart(val1,val2){
		    			    
				    new RGraph.Pie({
				        id: 'cvs',
				        data: [val1,val2],
				        options: {
				            linewidth: 0,
				            strokestyle: 'rgba(0,0,0,0)',
				            labelsIngraph:[val1, val2],
				            labelsIngraphBounding:false,
				            labelsIngraphSize:8,
				            labelsIngraphColor:'white',
				            labelsIngraphUnitsPre:'$',
				            colors: ['#3366CC','#DC3912'],
				            variant: 'pie3d',
				            radius: 50,
				            shadowOffsety: 5,
				            shadowColor: '#aaa',
				            exploded: [,10]
				        }
				    }).draw();
			}
